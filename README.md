# RDU Logic - Marrakech 

Final Project for Logic Programming Course 2021

Faculty of Computer Science, University of Indonesia

**Created By - RDU Logic**:

- Rafif Taris (1706979436)
- Gusti Ngurah Yama Adi Putra (1706979253)
- Raihansyah Attallah Andrian (1706040196)

## What is Marrakech?

Marrakech is a multiplayer board game that is played with the format of 1 VS All. In Marrakech each player takes the role of a rug salesperson who tries to outwit the competition. Each player starts with 10 coins (5 silver + 5 gold = 30 in total values) and an equal number of carpets.

On your turn, you may rotate Assam (the salesperson) 90 degrees. Then roll the die and move him forward as many spaces as showing (d6: 1, 2, 2, 3, 3, 4). If Assam reaches the edge of the board, follow the curve and continue moving in the next row. 

If Assam lands on another player's carpet, you must pay that player 1 coin per square showing that is contiguous with the landed-on square. Then, you place one of your carpets orthogonally adjacent to Assam (but may not directly overlay another carpet).

The game ends when all players have played all carpets. Each gets 1 coin per visible square. The player with most coins wins!

![alt text](image1.jpg)

![alt text](image2.jpg)

For more detailed images of Marrakech visit [this link][1].

## Project Scope

In this project, we plan to create an implementation of Marrakesh by using **Prolog**. In the program, we will create a **7x7 grid** as the Marrakech board. Also, we will  implement the **curves** that are on the edges of the board (as can be seen from the image above).

We plan to create a simplified version of the game with **only 2 players** (originally 4). There will be only **10 turns per person** (10 rugs per person) compared to the original 30 turns. These adjustments however can also be changed freely by changing a few lines of code.

As for the coins, we will also use the **same value of 30 per person**. However, all the coins will be **treated as silver coins** (1 silver = 1 value). This is just to simplify the game further.

The last adjustment is regarding the rule that when you put a carpet you have to not directly overlay another carpet. We decided to **not apply this rule** on our Prolog version of Marrakesh since it is quite hard to apply that rule.

## Running The Executables (for windows)

1. Go to "exe" directory
2. Run the "Marrakech.exe" file

## File Description

*  Marrakech
*  movement
*  payment
*  putRug
*  listReplaceHelper


## References

- Pac-Man Game with Prolog and Python [here][2].


[1]:https://boardgamegeek.com/boardgame/29223/marrakech
[2]:https://github.com/kajornsakp/prologProject