/* List replace elem code of Marrakech game.
 *  Implemented by:
 *  Rafif Taris (1706979436)
 *  Gusti Ngurah Yama Adi Putra (1706979253)
 *  Raihansyah Attallah Andrian (1706040196)
 */
:- module(listReplaceHelper,[replace_row_col/5]).

/* Rule to replace an element of 1D list
 *
 * replace_nth(N,Input,Value,Output)
 * 
 * {N} - The index of the list that should be replaced
 * {Input} - The input list
 * {Value} - Tha new value that is inserted in the list
 * {Output} - The output list result after replacement
 * 
 * Implementation with nth0(Index,List,Elem,Rest).
 * Where Rest is the remaining of the List after Elem is taken out from List at Index.
 * 
 * Simply take Rest of original input I and save as T (remove index N).
 * Then add new value V to T at index N and save as output O.
 */
replace_nth(N,I,V,O) :-
    nth0(N,I,_,T),
    nth0(N,O,V,T).

/* Rule to replace an element of 2D list
 * 
 * replace_row_col(Before,Row,Col,Data,After)
 * 
 * {Before} - The original 2D list
 * {Row} - The row of the list to be replaced
 * {Col} - The col of the list to be replaced
 * {Data} - The new data to replace index(row,col) of list
 * {After} - The resulting 2D list
 * 
 * Use nth0/3 to acces the Row from Before and save as Old.
 * Old is a 1D list.
 * Call replace_nth to change index Col of Old with Data,
 * save as Update (1D List after replaced).
 * 
 * Note that Before is also a 1D list that consists of lists as elements.
 * So we can call replace_nth to change the Row from Before to Update,
 * this will change the Row from Old to Update.
 * Finally, save the new 2D list with Update as After.
 */
replace_row_col(Before,Row,Col,Data,After) :-
    nth0(Row,Before,Old),
    replace_nth(Col,Old,Data,Update),
    replace_nth(Row,Before,Update,After).