/* Payment code of Marrakech game.
 *  Implemented by:
 *  Rafif Taris (1706979436)
 *  Gusti Ngurah Yama Adi Putra (1706979253)
 *  Raihansyah Attallah Andrian (1706040196)
 */
:- module(payment,[cekBayar/0]).
:- use_module(listReplaceHelper).

% ========== DFS CODE ==========

/* The function to be called in order to count connected area of a value using DFS
 *
 * dfsHelper(X,Y,Cek,Ans)
 * 
 * {X} - The starting column index
 * {Y} - The starting row index
 * {Cek} - The value that wants to be checked to count the connected area
 * {Ans} - The resulting area with value Cek starting from (X,Y)
 * 
 * Implementation simply takes the current board,
 * copy it to another term List,
 * and call DFS from (X,Y) with List.
 */
dfsHelper(X,Y,Cek,Ans):-
    board(Board),
    copy_term(Board,List),
    dfs(X,Y,Cek,List,_,Ans).

/* The DFS function to traverse board and count the connected area
 * 
 * dfs(X,Y,Cek,ListBefore,ListAfter,Ans)
 * 
 * {X} - The starting column index
 * {Y} - The starting row index
 * {Cek} - The value that wants to be checked to count the connected area
 * {ListBefore} - The list before traversal
 * {ListAfter} - The list after traversal
 * {Ans} - The resulting area with value Cek starting from (X,Y)
 * 
 * We need to save list before and after because during traversal
 * we will mark which locations we have visited before to not double count.
 * 
 * There are 4 base cases which is simply when a boundary is met.
 * In this case, the boundary is when an index has reached -1 or 7
 * since the board size is 7x7 (index 0-6).
 * On each basecase, simply state that Ans is 0 and cut 
 * so it will return to the calling function immediately.
 * 
 * Then for the inner parts of the board, there are 2 cases.
 * Case 1: When the current elem at (X,Y) is different than Cek
 * Case 2: When the current elem at (X,Y) is the same as Cek
 * 
 * For Case 1, simply just cut so it will backtrack.
 * For case 2, since it is the same, we need to traverse deeper with dfs.
 * First we mark (X,Y) as visited by calling replace_row_col and changing value to 'v'
 * Then we call dfs, and the traversal is done to all four possibilities:
 * up, down, left, right.
 * 
 * Notice that each dfs must be done sepparately and will change the board every time.
 * So we use the ListAfter of each dfs call as ListBefore of the next dfs call.
 * The final ListAfter (DList) will be the same as the ListAfter output of the current DFS call.
 * 
 * Finally, Ans will be equal to all dfs Ans call added by one for the current cell.
 */
dfs(-1,_,_,List,List,Ans) :- Ans is 0, !.
dfs(_,-1,_,List,List,Ans) :- Ans is 0, !.
dfs(7,_,_,List,List,Ans) :- Ans is 0, !.
dfs(_,7,_,List,List,Ans) :- Ans is 0, !.

dfs(X,Y,Cek,List,List,0) :-
    nth0(Y,List,Row),
    nth0(X,Row,Elem),
    Elem \= Cek, !.

dfs(X,Y,Cek,List,DList,Ans) :-
    nth0(Y,List,Row),
    nth0(X,Row,Elem),
    Elem = Cek, !,

    replace_row_col(List,Y,X,v,NewList),

    Left is X-1,
    Up is Y+1,
    Right is X+1,
    Down is Y-1,

    dfs(Left,Y,Cek,NewList,LList,L),
    dfs(X,Up,Cek,LList,UList,U),
    dfs(Right,Y,Cek,UList,RList,R),
    dfs(X,Down,Cek,RList,DList,D),

    Ans is L+U+R+D+1.

% ========== PAYMENT CODE ==========

/* The function to check if player needs to pay or not
 * 
 * No parameters.
 * 
 * Firstly we need to take the current cell value.
 * This can be done by taking the current position X,Y from assam state
 * and using nth0 to access the board.
 * 
 * Then, check if the value is the same as the current Player id or not.
 * 
 * From here there are two cases:
 * Case 1: If the rug belongs to the current player or still empty ('x')
 * Case 2: If the rug belongs to another player
 * 
 * If case 1 then just simply skip.
 * If case 2, then the rug must not also be 'x' (empty cell).
 * If so. then find connected area by calling dfsHelper predicate.
 * Then do payment by using bayar predicate for the current Player.
 */
cekBayar :-
    assam(X,Y,_,Player),
    board(List),
    nth0(Y,List,Row),
    nth0(X,Row,Rug),

    Rug \= x,
    Player \= Rug, !,

    dfsHelper(X,Y,Rug,Jumlah),
    writef('\nPlayer %w must pay %w point to Player %w before place Rug\n\n',[Player,Jumlah,Rug]),
    bayar(Player, Rug, Jumlah).

cekBayar :-
    assam(_,_,_,Player),
    writef('\nPlayer %w free to place Rug \n\n',[Player]). 
 
/* The function to make payment for a player to another player
 * 
 * bayar(Player1, Player2, Jumlah)
 * 
 * {Player1} - The player id that needs to pay (lose score)
 * {Player2} - The player id to be paid (gain score)
 * {Sum} - The sum to pay
 * 
 * There are two case:
 * Case 1: If Player1 has sufficient score
 * Case 2: If Player1 has no sufficient score
 * 
 * In case 1, simply deduct Jumlah from Player1 score fact,
 * then add Jumlah to Player2 score fact.
 * 
 * In case 2, Player1 has lost, so we assert the lose(Player1) fact.
 */
bayar(Player1, Player2, Sum) :-
    score(Player1,Balance1),
    Balance1 >= Sum, !,
    Res1 is Balance1 - Sum,
    retract(score(Player1,_)),
    assert(score(Player1,Res1)),

    score(Player2, Balance2),
    Res2 is Balance2 + Sum,
    retract(score(Player2,_)),
    assert(score(Player2,Res2)).

bayar(Player1, _, Sum) :-
    score(Player1,Balance1),
    Balance1 < Sum,
    retract(score(Player1,_)),
    assert(score(Player1,0)),
    lose(X),
    retract(lose(X)),
    assert(lose(Player1)).