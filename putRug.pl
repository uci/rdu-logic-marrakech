/* Put Rug code of Marrakech game.
 *  Implemented by:
 *  Rafif Taris (1706979436)
 *  Gusti Ngurah Yama Adi Putra (1706979253)
 *  Raihansyah Attallah Andrian (1706040196)
 */
:- module(putRug,[putRug/0,writeGridHelper/0]).
:- use_module(listReplaceHelper).
:- use_module(interface).

% ========== Facts for Rug Placement ==========
/* Facts for available Rug Placement
 * 
 * getNewXY(OldDir, RelDir, ResDir, X0, Y0, X1, Y1).
 * 
 * {OldDir} - Direction before adjustment
 * {RelDir} - Relative direction from an object perspective
 * {ResDir} - Resulting direction after adjustment from relative persepective
 * {X0} - X coordinate before object (carpet) placement
 * {Y0} - Y coordinate before object (carpet) placement
 * {X1} - X coordinate after object (carpet) placement
 * {Y1} - Y coordinate after object (carpet) placement
 */
getNewXY(up, front, up, X, Y, X, Y-1).
getNewXY(up, left, left, X, Y, X-1, Y).
getNewXY(up, right, right, X, Y, X+1, Y).

getNewXY(left, front, left, X, Y, X-1, Y).
getNewXY(left, left, down, X, Y, X, Y+1).
getNewXY(left, right, up, X, Y, X, Y-1).

getNewXY(right, front, right, X, Y, X+1, Y).
getNewXY(right, left, up, X, Y, X, Y-1).
getNewXY(right, right, down, X, Y, X, Y+1).

getNewXY(down, front, down, X, Y, X, Y+1).
getNewXY(down, left, right, X, Y, X+1, Y).
getNewXY(down, right, left, X, Y, X-1, Y).

% ========== PUT RUG CODE ==========

% ========== Get Direction Choices ==========
/* Rule to validate whether relative direction 
 * is available as a choice to player
 *
 * validateDirection(X,Y,CurrentDir,Relative)
 * 
 * {X} - X coordinate of the object
 * {Y} - Y coordinate of the object
 * {CurrentDir} - Current direction where the object is facing
 * {Relative} - Available relative direction for player's choice
 * 
 * Get the getNewXY facts then check whether the new coordinates
 * is still inside of the board or not.
 */
validateDirection(X,Y,CurrentDir,Relative):-
    getNewXY(CurrentDir,Relative,_,X,Y,TempX,TempY),
    NewX is TempX, NewY is TempY,  % Needed to compute mathematical operation
    NewX >= 0, NewX =< 6,
    NewY >= 0, NewY =< 6.

/* Rule to obtain all available options of direction
 *
 * availableDirection(X,Y,CurrentDir,AvailDirs)
 * 
 * {X} - X coordinate of the object
 * {Y} - Y coordinate of the object
 * {CurrentDir} - Current direction of the object
 * {AvailDirs} - All available options of direction for player's choice accumulated in a list
 * 
 * Gather all relative directions that fulfills the validateDirection rule
 * and save them in AvailDirs list.
 */
availableDirection(X,Y,CurrentDir,AvailDirs) :-
    findall(Relative,validateDirection(X,Y,CurrentDir,Relative),AvailDirs).

% ========== Main Put Rug Predicate ==========
/* Rule to define the flow of put rug
 *
 * There are several steps to put a rug
 * 1. Ask player to choose first carpet position based on direction relative to Assam
 * 2. Ask player to choose second carpet position based on direction relative to first carpet
 * 3. Replace tiles with carpet based on players input
 * 4. Save the state of new board
 * 5. Display the board
 */ 
putRug :-
    assam(X,Y,CurrentDir,_),
    availableDirection(X,Y,CurrentDir,AvailDirs),
    askDirection('Choose direction 1 (based on assam direction):','Assam',AvailDirs,Dir1),

    board(List),
    assam(X,Y,InitDir,Player),

    getNewXY(InitDir, Dir1, NewDir, X, Y, TempX, TempY),
    % Needed to compute mathematical operation
    NewX is TempX,
    NewY is TempY,

    availableDirection(NewX,NewY,NewDir,AvailDirs2),
    askDirection('\nChoose direction 2 (based on first carpet direction):','carpet',AvailDirs2,Dir2),

    getNewXY(NewDir, Dir2, _, NewX, NewY, Temp2X, Temp2Y),
    % Needed to compute mathematical operation
    New2X is Temp2X,
    New2Y is Temp2Y,

    replace_row_col(List, NewY, NewX, Player, List2),
    replace_row_col(List2, New2Y, New2X, Player, NewBoard),

    retract(board(_)),
    assert(board(NewBoard)),
    
    write('\n===== AFTER PUT RUG =====\n'),
    writeGridHelper.
