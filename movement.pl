/* Movement code of Marrakech game.
 *  Implemented by:
 *  Rafif Taris (1706979436)
 *  Gusti Ngurah Yama Adi Putra (1706979253)
 *  Raihansyah Attallah Andrian (1706040196)
 */
:- module(movement,[setDirection/1,moveAssam/1,getAssamDirectionChoice/2]).

% ========== CONSTANTS ==========

/* Facts to indicate what valid directions are available
 *
 * assamRotation(Before,After).
 *
 * {Before} - Direction before choosing new direction
 * {After} - Available directions if want to change
 *
 * If Assam is facing up or down, then Assam can go left or right.
 * Vice versa if Assam is facing left or right.
 */
assamRotation([up,down],[left,right]).
assamRotation([left,right],[up,down]).

/* Facts to indicate fixed pos U-Turns of all sides on the board.
 *
 * uTurn(Dir, List).
 * 
 * {Dir} - Direction of Assam entering a U-Turn
 * 
 * {List} - Consist of tuples [A,B]
 * A indicates the entry coord before the U-Turn.
 * B indicates the exit coord after the U-Turn.
 * Coord are fixed, thus will be called "fixed coord".
 * Coord can be X or Y depending on Dir.
 * If Dir is left or right then use Y coord.
 * Else Dir is up or down then use X coord.
 */
uTurn(left,[[0,0],[1,2],[3,4],[5,6]]).
uTurn(right,[[0,1],[2,3],[4,5],[6,6]]).
uTurn(up,[[0,0],[1,2],[3,4],[5,6]]).
uTurn(down,[[0,1],[2,3],[4,5],[6,6]]).

/* Facts to mark opposite direction after entering a U-Turn
 * 
 * oppositeDirection(P,Q).
 * 
 * {P} is the direction after entering a U-Turn with direction {Q}
 * Vice versa for {Q} after entering with direction {P}.
 * 
 */ 
oppositeDirection(up,down).
oppositeDirection(left,right).

% ========== MOVEMENT CODE ==========
/* Rule to take available directions from current direction.
 *
 * getAssamDirectionChoice(CurDir,AvailDirs)
 * 
 * {CurDir} - Current direction of Assam
 * {AvailDirs} - Available direction options for player to change (including CurDir)
 * 
 * To obtain available option of directions, we use the assamRotation facts.
 * Cut is used to prevent check (unification) to another direction that is member of Before.
 */
getAssamDirectionChoice(CurDir,[CurDir|AvailDirs]) :-
    assamRotation(Before,AvailDirs),
    member(CurDir,Before),!.

/* Rule to change direction of Assam.
 *
 * setDirection(NewDir)
 *
 * NewDir - One of the following directions [up,down,left,right].
 *
 * Implemented with 2 rules.
 * First rule is when NewDir is already the same
 * as Assams' direction. We just need to cut the second rule.
 *
 * Second rule, we use the assamRotation facts to check
 * if NewDir can be chosen and finally we create the new 
 * fact for Assam.
 */
setDirection(NewDir):- assam(_,_,NewDir,_), !.
setDirection(NewDir):-
    assam(_,_,CurrentDir,_),
    assamRotation(Before,After),
    member(CurrentDir,Before),
    member(NewDir,After),
    retract(assam(X,Y,_,CurrentPlayer)),
    assert(assam(X,Y,NewDir,CurrentPlayer)).

/* Rule to move Assam based on Steps
 *
 * moveAssam(Steps)
 * 
 * {Steps} - Number (int) of steps Assam wants to move.
 * 
 * Predicate will move Assam by {Steps} ammount
 * by looking at Dir in assam(_,_,Dir,_) fact.
 * 
 * Only one coordinate will change:
 * Moving up or down changes y coord (x is fixed)
 * Moving left or right changes x coord (y is fixed)
 * 
 * Only one of the predicates will be called when they match
 * the correct Dir and all others will be cut.
 * 
 * After successfully moving, retract the previous fact
 * and assert the new fact.
 * 
 * Then, call checkBoundary to check if assam has moved outside
 * the board (i.e. enter a U-Turn).
 */
moveAssam(Steps):-
    assam(X,Y,up,CurrentPlayer),!,
    NewY is Y-Steps,
    retract(assam(_,_,_,_)),
    assert(assam(X,NewY,up,CurrentPlayer)),
    checkBoundary.

moveAssam(Steps):-
    assam(X,Y,down,CurrentPlayer),!,
    NewY is Y+Steps,
    retract(assam(_,_,_,_)),
    assert(assam(X,NewY,down,CurrentPlayer)),
    checkBoundary.

moveAssam(Steps):-
    assam(X,Y,left,CurrentPlayer),!,
    NewX is X-Steps,
    retract(assam(_,_,_,_)),
    assert(assam(NewX,Y,left,CurrentPlayer)),
    checkBoundary.

moveAssam(Steps):-
    assam(X,Y,right,CurrentPlayer),
    NewX is X+Steps,
    retract(assam(_,_,_,_)),
    assert(assam(NewX,Y,right,CurrentPlayer)),
    checkBoundary.

/* Rule to find opposite dir of X as Y
 * 
 * By using the facts of oppositeDirection.
 * 
 * Either oppositeDirection(X,Y) or
 * oppositeDirection(Y,X) is true.
 * 
 * So we explore both possibilities with cut on the first one
 * to not go to the second if it is indeed case one.
 */
getOppositeDir(X,Y):- oppositeDirection(X,Y),!; oppositeDirection(Y,X).

/* Rules to find the new direction after entering a U-Turn
 * 
 * NewDir depends on the last square Assam visit before entering U-Turn.
 * 
 * getNewDirection(Dir,Pos,NewDir)
 * 
 * {Dir} - The old direction of Assam
 * {NewDir} - The new direction of Assam
 * {Pos} - The coordinate where Assam entered the U-Turn,
 *         Pair of integers
 * 
 * There are 4 special U-Turns which are the
 * top left corner [0,0] and right bottom corner [6,6].
 * These squares are excluded on special rules by themselves.
 * 
 * As for other U-Turns we can simply call the getOppositeDir predicate.
 */
getNewDirection(up,[0,0],right) :- !.
getNewDirection(left,[0,0],down) :- !.
getNewDirection(down,[6,6],left):- !.
getNewDirection(right,[6,6],up):- !.
getNewDirection(Dir,_,NewDir):- getOppositeDir(Dir,NewDir).

/* Rule to find the new fixed pos and new direction after entering U-Turn
 *
 * Fixed pos - either the x or y coord that is mapped by uTurn facts
 * 
 * getPair(Old,Dir,Tuples,NewPos,NewDir)
 * 
 * {Old} - The old fixed position before entering U-Turn
 * {Dir} - The old direction before entering U-Turn
 * {Tuples} - The available U-Turns from uTurn facts
 * {NewPos} - The new fixed position after entering U-Turn
 * {NewDir} - The new direction after entering U-Turn
 * 
 * Implementation simply iterate all tuples in the list {Tuples}.
 * For each member, check if Old is in the current tuple CurTup, if so get index and cut
 * to not go to other possibilities.
 * 
 * Acess the mapped value of Old as NewPos from CurTup.
 * Then use getNewDirection rules to find NewDir using Dir and CurTup.
 * 
 * Note: Use Old fixed pos and Dir to get new fixed pos and new dir.
 */
getPair(Old,Dir,Tuples,NewPos,NewDir) :-
    member(CurTup,Tuples),
    nth0(Index,CurTup,Old),!,
    NewIndex is (Index+1) mod 2,
    nth0(NewIndex,CurTup,NewPos),
    getNewDirection(Dir,CurTup,NewDir).

/* Rules to count how many steps are overflowed and non fixed pos after entering U-Turn
 *
 * Non fixed pos - either the x or y coord that is not mapped by uTurn facts
 * 
 * getExtraSteps(Pos,Res,Steps)
 * 
 * {Pos} - The initial pos after overflowed (< 0  or > 6)
 *         Pos is only one integer (not pair) marking the
 *         location that is not fixed by moveAssam.
 *         (Moving up or down has fixed x coord,
 *          Moving left or right has fixed y coord)
 * {Res} - The result pos after overflowed (0 or 6)
 * {Steps} - How many more steps to take after overflowed
 * 
 * Can be divided into two cases based on direction before entering U-Turn:
 * (Left or Top) and (Right or Bottom)
 * 
 * Rule 1 is for Left or Top case.
 * In this case, Res will always be 0.
 * To validate this case simply check if 
 * it is indeed overflowing to the left.
 * If so then cut to prevent going to second case 
 * and count remaining steps.
 * 
 * Rule 2 is for Right or Bottom case.
 * In this case, Res will always be 6.
 * Simply just count remaining steps.
 * 
 * Steps is reduced by one as we already count the resulting
 * square after the U-Turn.
 * 
 * Use Pos to find Res and Steps of that Pos.
 * Note: Assam has not moved to the square he should land on.
 */
getExtraSteps(Pos,0,Steps):-
    LowDiff is 0 - Pos,
    HighDiff is Pos - 6,
    LowDiff > HighDiff,!,
    Steps is LowDiff - 1.

getExtraSteps(Pos,6,Steps):-
    Diff is Pos - 6,
    Steps is Diff - 1.

/* Main function to make U-Turn when moving Assam.
 *
 * Can be divided into two cases:
 * When Dir of Assam is left or right,
 * When Dir of Assam is up or down.
 * 
 * Case 1: Use Y for fixed pos, X for not fixed pos
 * Case 2: Uses X for fixed pos, Y for not fixed pos
 * 
 * Similar for both cases, after checking the Dir of Assam,
 * cut so the other option is not called.
 * 
 * Then twe use uTurn facts to get all possible Tuples of Dir.
 * Use getPair with X/Y value, Dir, and Tuples to get NewX/NewY and NewDir.
 * Use getExtraSteps with Y/X value to get NewY/NewX and Steps.
 * 
 * Finally, we can retract the old assam fact and 
 * assert the new assam state using NewX/NewY, NewY/NewX, NewDir, and CurrentPlayer.
 * 
 * Last, we need to move assam again based on Steps to move assam to its rightful position.
 */
makeUTurn:-
    assam(X,Y,Dir,CurrentPlayer),
    member(Dir,[left,right]),!,

    uTurn(Dir,Tuples),
    getPair(Y,Dir,Tuples,NewY,NewDir),
    getExtraSteps(X,NewX,Steps),

    retract(assam(_,_,_,_)),
    assert(assam(NewX,NewY,NewDir,CurrentPlayer)),
    moveAssam(Steps).

makeUTurn:-
    assam(X,Y,Dir,CurrentPlayer),

    uTurn(Dir,Tuples),
    getPair(X,Dir,Tuples,NewX,NewDir),
    getExtraSteps(Y,NewY,Steps),

    retract(assam(_,_,_,_)),
    assert(assam(NewX,NewY,NewDir,CurrentPlayer)),
    moveAssam(Steps).

/* Rules to check if Assam is out of the board
 *
 * Divided into 4 cases as there are 4 sides of the board.
 * Each rule checks one side of the board.
 * 
 * We use cut to remove all other calling of checkBoundary
 * if one of the conditions is met.
 * 
 * If any conditions is met, we call makeUTurn to move Assam.
 * 
 * Else when no conditions is met (i.e. no overflow),
 * we simply state that checkBoundary is True.
 */
checkBoundary:-
    assam(_,Y,_,_),
    Y < 0,!,
    makeUTurn.
checkBoundary:-
    assam(_,Y,_,_),
    Y > 6,!,
    makeUTurn.
checkBoundary:-
    assam(X,_,_,_),
    X < 0,!,
    makeUTurn.
checkBoundary:-
    assam(X,_,_,_),
    X > 6,!,
    makeUTurn.
checkBoundary.