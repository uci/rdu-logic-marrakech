/* Main Program of Marrakech game.
 *  Implemented by:
 *  Rafif Taris (1706979436)
 *  Gusti Ngurah Yama Adi Putra (1706979253)
 *  Raihansyah Attallah Andrian (1706040196)
 */
:- use_module(putRug).
:- use_module(payment).
:- use_module(movement).
:- use_module(interface).

:- abolish(lose/1).
:- abolish(board/1).
:- abolish(assam/4).
:- abolish(score/2).
:- abolish(currentRound/1).
:- dynamic lose/1.
:- dynamic board/1.
:- dynamic assam/4.
:- dynamic score/2.
:- dynamic currentRound/1.

% ========== CONSTANTS ==========
% Lose fact. Null will be replaced by player number 
% if that player fulfills the lose condition.
lose(null).

% Board FOR DEMO PURPOSES: turn abis, bangkrut
% board([[x,x,x,x,x,x,x],
%        [x,x,x,0,x,x,x],
%        [x,x,1,0,0,1,1],
%        [x,x,1,1,1,1,1],
%        [x,x,x,0,1,1,1],
%        [x,x,1,1,0,0,1],
%        [x,x,x,x,x,0,1]]).

% Initial Board
board([[x,x,x,x,x,x,x],
       [x,x,x,x,x,x,x],
       [x,x,x,x,x,x,x],
       [x,x,x,x,x,x,x],
       [x,x,x,x,x,x,x],
       [x,x,x,x,x,x,x],
       [x,x,x,x,x,x,x]]).

/* The character position as fact:
 *
 * assam(Row,Col,Direction,PlayerId)
 *
 * {Row} - marks the row of Assams' position (first index or x)
 * {Col} - marks the column of Assams' position (second index or y)
 * The initial location is [3,3] (center of board).
 *
 * {Direction} - is either one of [up,down,left,right].
 * Up: Move one row up (y-1)
 * Down: Move one row down (y+1)
 * Left: Move one col left (x-1)
 * Right: Move one col right (x+1)
 *
 * {PlayerId} is the id of the current player taking a turn.
 * Initially 0 for player 0.
 * Originally can only be either 0 or 1 (2 players).
 *
 * This fact will change every time Assam moves.
 */

assam(3,3,up,0).

/* Scores of each player.
 * 
 * score(PlayerId,Coins)
 *  
 * {PlayerId} is the id of the player for this score.
 *  
 * {Coins} initially is 30 for each player.
 */
% score(0,1). % FOR DEMO PURPOSES: bangkrut
score(0,30).
score(1,30).

/* The number of rounds to be played.
 * Initially 10 for all players.
 *
 * Incidentally, there will also be only 10 rugs
 * to be placed by each player.
 *
 * Current round will be decremented after the 2nd players'
 * round has finished.
*/
% currentRound(1). % FOR DEMO PURPOSES: Turn abis
currentRound(30).

% boardsize(N) Marks the size of the board as N x N
boardSize(7).

% playerCount(N) marks the number of players playing
playerCount(2).

% ========== PLAYER TURN CODE ==========

% Rule to roll the dice. Random between 1 and 4.
rollDice(Result) :- random_between(1,4,Result).

% Check if no player has lost.
cekNotLose(Player) :- \+lose(Player).

/* Rule to decrement available turns.
 * 
 * decTurn(Player)
 * 
 * {Player} - Current player number
 * 
 * Turns is decreased whenever
 * Player 1 finished their turn.
 */
decTurn(Player) :-
       Player = 1,
       retract(currentRound(N)),
       M is N-1,
       assert(currentRound(M)).
decTurn(_).

/* Rule to check available turn. 
 *
 * Implemented with 2 rules.
 * First rule is when there are some turns that is not played.
 *
 * Second rule, after there is no available turns,
 * show board, final scores, and the winner as the game ends.
 * Halt to fully close swipl after playing the game.
 */
cekTurn :-
       currentRound(X),
       X > 0, !.

cekTurn :- 
       % Print final score
       write('\n===== GAME ENDED =====\n'),
       writeGridHelper,
       write('\n'),
       writeScoreHelper,

       % Determine win
       winner(PlayerWin, PlayerLose),
       writef('Player %w Wins!\n',[PlayerWin]),
       writef('Player %w Lose...\n',[PlayerLose]),
       
       end.

/* Rule to determine winner by score comparison.
 *
 * winner(Win, Lose)
 * 
 * {Win} - Player number who wins the game
 * {Lose} - Player number who loses the game
 * 
 * Score compared based on the current facts and 
 * it is ensured that Win and Lose is different player.
 */  
winner(Win, Lose):-
       score(Win, X),
       score(Lose, Y),
       X >= Y,
       Win \= Lose.
       
/* Rule to take turn until all turns are over or one player loses.
 *
 * Implemented with 2 rules.
 * First rule is the main rule where every steps 
 * of a turn is defined (based on real game rules).
 * 
 * Every turn consists of:
 * 1. Check if there is available turns to play
 * 2. Show current state of the game
 * 3. Ask player to choose Assam's Direction then change his direction
 * 4. Roll die and move Assam forward based on the die
 * 5. Check if Assam is currently on an carpet of opposite player.
 *    Pay appropriate coins to other player if the condition fulfilled.
 * 6. Ask player to put rug on available spaces.
 * 7. Change to next player.
 * 
 * Generally, board condition is displayed after every step of a turn.
 * Whenever a player failed to pay, the second rule of takeTurn is used.
 * 
 * On the second rule, show the winner and loser of current game.
 * Halt to fully close swipl after playing the game.
 */
takeTurn :-
       % Check available turn
       cekTurn,

       % Current State
       writeCurrentState('CURRENT STATE'),

       assam(_,_,Dir,Player),
       writef('PLAYER %w\'s TURN\n',[Player]),
       getAssamDirectionChoice(Dir,AvailDirs),
       askDirection('Choose Assam\'s movement direction:','board',AvailDirs,NewDir),
       setDirection(NewDir),

       write('\n===== AFTER SET DIRECTION =====\n'),
       writeGridHelper,
       sleep(1),

       rollDice(Steps),
       writef('\n==> Your Dice Roll: %w\n',[Steps]),
       sleep(0.75),
       moveAssam(Steps),

       write('\n===== AFTER MOVEMENT =====\n'),
       writeGridHelper,
       sleep(0.75),

       write('\n===== PAYMENT =====\n'),
       cekBayar,
       cekNotLose(Player),!,
       writeScoreHelper,
       sleep(0.75),

       write('\n===== PUT RUG =====\n'),
       putRug,
       write('\n'),
       sleep(0.75),

       % Next Player
       Other is (Player + 1) mod 2,
       retract(assam(X,Y,DirLast,Player)),
       assert(assam(X,Y,DirLast,Other)),

       % Decrement turn
       decTurn(Player),

       % Continue
       takeTurn.

% When take turn for a player fails because unable to do payment, then that player loses
takeTurn :- 
       lose(Player),
       write('\n===== GAME ENDED =====\n'),
       Other is (Player + 1) mod 2,
       writef('Player %w Wins!\n',[Other]),
       writef('Player %w Lose by Bankruptcy....\n',[Player]),
       end.

% Rule to start the game
start :-
       write('====================\n'),
       write('==== WELCOME TO ====\n'),
       write('====  MARRAKECH ====\n'),
       write('====================\n'),
       write('\nGame Starting...\n\n'),
       sleep(1.5),
       takeTurn.

% Rule to end the game
end :- 
       write('\nClosing The Game...'),
       write('\nBye bye!\n'),
       write('\nType "close." to exit\n'),
       read(_),
       halt.