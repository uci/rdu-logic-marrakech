/* Interface code of Marrakech game.
 *  Implemented by:
 *  Rafif Taris (1706979436)
 *  Gusti Ngurah Yama Adi Putra (1706979253)
 *  Raihansyah Attallah Andrian (1706040196)
 */
:- module(interface,[askDirection/4,writeGridHelper/0,writeCurrentState/1,writeScoreHelper/0]).
:- use_module(listReplaceHelper).

% ========== INTERFACE CODE ==========

% ========== Directions ==========
/* Rule to display available options of direction
 *
 * writeDirectionChoices(Object,DirList)
 * 
 * {Object} - Relative object for the direction
 * {DirList} - Available directions in list
 * 
 * Access the head of DirList recursively until there is no available direction
 * while displaying the option as
 * "- [Direction] of [Object]"
 * In the end of recursive, show ">>> " as a sign to ask input
 */
writeDirectionChoices(_,[]) :- !, write('>>> ').
writeDirectionChoices(Object,[Dir|DirList]) :- 
    writef('- "%w" of %w\n',[Dir,Object]),
    writeDirectionChoices(Object,DirList).

/* Rule for asking player to give their choice of direction
 *
 * askDirection(Instruction,Perspective,Dirs,InputDir)
 * 
 * {Instruction} - Instruction (string) that is displayed before the available choices
 * {Perspective} - Relative object for the main perspective as a string
 * {Dirs} - List of available options of direction
 * {InputDir} - Player's inputted choice of direction
 * 
 * This rule implements failure driven loops by using built-in repeat rule.
 * First, show instruction and then show available options of directions using writeDirectionChoices rule.
 * After that, read player's inputted term and check if the inputted term is a member of available options.
 * If that condition is fulfilled, the repeat will be cutted.
 * Otherwise, tell the player that the direction is invalid and ask the direction again.
 */
askDirection(Instruction,Perspective,Dirs,InputDir) :-
    repeat,
        writef('%w\n',[Instruction]),
        writeDirectionChoices(Perspective,Dirs),
        read(InputDir),
        ( member(InputDir,Dirs), !;
        writef('\nInvalid Direction!\n'), fail
        ).

% ========== Write Grid ==========
/* Facts to display Assam on board
 *
 * arrow(Direction,[Arrow])
 * 
 * {Direction} - Direction atom
 * {Arrow} - Directed arrow symbol in ascii unicode
 */
arrow(left,[8592]).
arrow(up, [8593]).
arrow(right,[8594]).
arrow(down,[8595]).

/* Rule to help displaying board state
 *
 * writeGridHelper will check the current board state and current Assam state.
 * Then we retreive the arrow ascii of the current Dir of Assam.
 * Then we write the whole board by each row with the writeGrid rule after
 * replacing Assam's current position with the arrow direction.
 * 
 * writeGrid(Board, R)
 * 
 * {Board} - The current board in 2D list
 * {R} - An integer denotes the row to be printed
 * 
 * Implementation simply check if R is less than 6.
 * Then get the Row from Board and write it.
 * Afterwards, increment R and recurse to the next row.
 * Once R reaches 7, the predicate will return success.
 */
writeGridHelper :-
    board(Board),
    assam(X,Y,Dir,_),
    arrow(Dir,Ascii),
    name(Arrow, Ascii),
    replace_row_col(Board, Y, X, Arrow, Board2),
    writeGrid(Board2, 0).

writeGrid(Board, R) :-
    R =< 6, !,
    nth0(R,Board,Row),
    write(Row),
    write('\n'),
    NewR is R + 1,
    writeGrid(Board, NewR).

writeGrid(_,_).

% ========== Write Score ==========
/* Rules to help write score of all players
 * 
 * writeScoreHelper will find all score facts available to obtain PlayerList available and ScoreList.
 * This can be done with the builtin findall predicate.
 * Afterwards we write each pair of player id and score using writeScores rules.
 * 
 * writeScores(PlayerList,ScoreList)
 * 
 * {PlayerList} - List of available player ids
 * {ScoreList} - List of players' scores
 * 
 * Implementation of writeScore simply just take each head of the list and write them as a pair.
 * Then we recurse to the tail of both lists until list is empty (expected length of list is the same).
 * writeScores will have a base case of both lists in parameter empty that is always true.
 */
writeScores([],[]) :- !.
writeScores([Player|PlayerList],[Score|ScoreList]) :- 
    writef('- Player %w: %w\n',[Player,Score]),
    writeScores(PlayerList,ScoreList).
writeScoreHelper :- 
    findall(Player,score(Player,_),PlayerList),
    findall(Score,score(_,Score),ScoreList),
    writef('Player\'s Coin:\n'),
    writeScores(PlayerList,ScoreList).

% ======= Game Element States ========
/* The rule to write current state of the game as a whole
 * 
 * writeCurrentState(Desc)
 * 
 * {Desc} - A string that is the description for the state to be printed
 * 
 * writeCurrentState can be used to print the whole state of the game using previously explained predicates.
 * First, we will write the current grid with writeGridHelper.
 * Second, we print Assam's current coordinate and direction from assam fact.
 * Next, we print the current player who is taking this turn also from assam fact.
 * Then we print the list of players and scores using writeScoreHelper.
 * Finally, we write how many turns are left for all players using the currentRound fact.
 */
writeCurrentState(Desc) :-
    writef('===== %w =====\n',[Desc]),
    assam(X,Y,Dir,CurPlayer),
    
    currentRound(Turn),
    
    writeGridHelper,
    
    writef('Assam\'s Coordinate: (%w,%w)\n',[X,Y]),
    writef('Assam\'s Direction: %w of board\n',[Dir]),
    writef('Current Player: Player %w\n',[CurPlayer]),
    writeScoreHelper,
    writef('\nRound(s) Before Game End: %w\n\n',[Turn]).